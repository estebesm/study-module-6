import './App.scss';
import {Route, Routes, useLocation, useNavigate} from "react-router-dom";
import Menu from "./pages/menu/Menu";
import Auth from "./pages/auth/Auth";
import {useEffect, useState} from "react";
import {
    addUsernameToLocalStorage,
    getUsernameFromLocalStorage,
    removeUsernameFromLocalStorage
} from "./functions/localStorage";
import Game from "./pages/game/Game";
import Leaderboard from "./pages/leaderboard/Leaderboard";

function App() {
    const [authenticated, setAuthenticated] = useState(!!getUsernameFromLocalStorage())
    const navigate = useNavigate()
    const location = useLocation()

    const signOut = () => {
        removeUsernameFromLocalStorage()
        setAuthenticated(false)
    }
    const signIn = username => {
        addUsernameToLocalStorage(username)
        setAuthenticated(true)
    }

    useEffect(() => {
        if(location.pathname !== '/auth'){
            navigate('/auth')
        }
    }, [])
    useEffect(() => {
        if(authenticated){
            navigate('/')
        } else {
            navigate('/auth')
        }
    }, [authenticated])

  return (
        <Routes>
            <Route exact path='/auth' element={<Auth signIn={signIn} authenticated={authenticated}/>}/>
            <Route exact path='/game/:mode' element={<Game/>}/>
            <Route exact path='/leaderboard' element={<Leaderboard/>}/>
            <Route exact path='/' element={<Menu signOut={signOut}/>}/>
        </Routes>
  );
}

export default App;

import React from 'react';
import './authForm.scss'
import {useForm} from "react-hook-form";
import {addUsernameToLocalStorage} from "../../functions/localStorage";


const AuthForm = (props) => {
    const {signIn} = props

    const { register, handleSubmit, formState: { errors }, reset} = useForm();
    const onSubmit = data => {
        signIn(data.username)
        reset();
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)} className={'authForm'}>
            <label htmlFor="username"/>
            <input
                id="email"
                {...register("username", {
                    required: "required",
                    pattern: {
                        value: /^[a-zA-Z\-]+$/,
                        message: 'username should have only A-z and "-" symbol'
                    }
                })}
                type="text"
                placeholder={'username'}
            />
            {errors.username && <span role="alert">{errors.username.message}</span>}
            <button type="submit">Sign in</button>
        </form>
    );
};

export default AuthForm;
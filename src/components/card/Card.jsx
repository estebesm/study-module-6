import React from 'react';
import './card.scss'

const Card = (props) => {
    const {activeCards,
        setActiveCards,
        foundCards,
        disabled,
        card,
        index} = props
    const active = activeCards.includes(index) || foundCards.includes(index)
    return (
        <button className={`card ${active ? 'active' : ''}`}
             disabled={active || disabled}
             onClick={() => {
                 setActiveCards([...activeCards, index])
             }}
        >
            <div className="card-side front">
                <img src={require(`../../images/cards/game-card-0.png`)} alt={'card'}/>
            </div>
            <div className="card-side back">
                <img src={require(`../../images/cards/game-card-${card}.png`)} alt={'card'}/>
            </div>
        </button>
    );
};

export default Card;
import React, {useState, useEffect} from 'react';
import './gameMenu.scss'
import {useNavigate} from "react-router-dom";
import pause from "../../images/pause-button.png"

const GameMenu = ({restartGame}) => {
    const navigate = useNavigate()

    const [opened, setOpened] = useState(false)
    const closeMenu = () => {
        setOpened(false)
    }
    useEffect(() => {
        if(!opened){
            document.body.style.overflow = 'hidden'
        }
        else {
            document.body.style.overflow = 'visible'
        }
    }, [opened])

    return (
        <>
            <button onClick={() => setOpened(true)} className={'stop-game'}>
                <img src={pause} alt={'pause'} />
            </button>
            <div className={`modal__wrapper ${opened ? 'active' : ''}`}
                 onClick={(e) => {
                     e.stopPropagation()
                     closeMenu()
                 }}
            >
                <div className={'modal'}>
                    <div className={'container'}>
                        <div className={'modal__content'}
                             onClick={e => {
                                 e.stopPropagation()
                             }}
                        >
                            <h3>
                                PAUSE
                            </h3>
                            <button onClick={closeMenu}>
                                RESUME
                            </button>
                            <button onClick={() => {
                                closeMenu()
                                restartGame()
                            }}>
                                TRY AGAIN
                            </button>
                            <button onClick={() => {
                                closeMenu()
                                navigate('/')
                            }}>
                                MENU
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default GameMenu;
import React, {useEffect} from 'react';
import './resultWindow.scss'
import {useNavigate} from "react-router-dom";

const ResultWindow = ({opened, closeWindow, steps, restartGame, time}) => {
    const navigate = useNavigate()
    useEffect(() => {
        if(opened){
            document.body.style.overflow = 'hidden'
        }
        else {
            document.body.style.overflow = 'visible'
        }
    }, [opened])
    return (
        opened ?
            <div className={'modal__wrapper active'} onClick={ e => {
                e.stopPropagation()
            }}>
                <div className={'modal'}>
                    <div className={'container'}>
                        <div className={'modal__content'}
                             onClick={e => {
                                 e.stopPropagation()
                             }}
                        >
                            <h3>
                                YOUR SCORE: {steps * time}
                            </h3>
                            <button onClick={() => {
                                closeWindow()
                                restartGame()
                            }}>
                                TRY AGAIN
                            </button>
                            <button onClick={() => {
                                closeWindow()
                                navigate('/leaderboard')
                            }}>
                                LEADERBOARD
                            </button>
                            <button onClick={() => {
                                closeWindow()
                                navigate('/')
                            }}>
                                MENU
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            : <></>
    );
};

export default ResultWindow;
import React, {useState} from 'react';
import './sortWindow.scss'
import sortImage from '../../images/sort.png'
import {getLeaderboard, getLeaderboardSortedBySteps, getLeaderboardSortedByTime} from "../../functions/localStorage";

const SortWindow = ({setLeaderBoard, activeSortItem, setActiveSortItem}) => {
    const [sortWindowOpened, setSortWindowOpened] = useState(false)
    const sortItems = [
        {
            name: 'score',
            sort: () => setLeaderBoard(getLeaderboard())
        },
        {
            name: 'time',
            sort: () => setLeaderBoard(getLeaderboardSortedByTime())
        },
        {
            name: 'steps',
            sort: () => setLeaderBoard(getLeaderboardSortedBySteps())
        }
    ]

    return (
        <div className={'sort-window'}>
            <button className={'open-sort'}
                    onClick={() => setSortWindowOpened(!sortWindowOpened)}
            >
                {activeSortItem}
                <img src={sortImage} alt={'sort'}/>
            </button>
            {sortWindowOpened &&
                <ul>
                    {
                        sortItems.map((item, index) =>
                            <li className={'title'}
                                key={`sort-item__${index}`}>
                                <button
                                    className={`${item.name === activeSortItem ? 'active' : ''}`}
                                    onClick={() => {
                                        setActiveSortItem(item.name)
                                        item.sort()
                                        setSortWindowOpened(false)
                                    }}
                                >
                                    {item.name}
                                </button>
                            </li>)
                    }
                </ul>
            }
        </div>
    );
};

export default SortWindow;
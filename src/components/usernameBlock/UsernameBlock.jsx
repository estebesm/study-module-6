import React, {useState} from 'react';
import './usernameblock.scss'
import {getUsernameFromLocalStorage} from "../../functions/localStorage";
import logoutImage from '../../images/logout.png'

const UsernameBlock = ({signOut, disabled}) => {
    const [logoutWindowOpened, setLogoutWindowOpened] = useState(false)
    return (
        <div className={'usernameBlock'}>
            <button disabled={disabled}
                    onClick={() => {setLogoutWindowOpened(!logoutWindowOpened)}}
                    className={'username'}>
                {getUsernameFromLocalStorage()}
                <img src={logoutImage} alt={'logout'}/>
            </button>
            {
                logoutWindowOpened ?
                <div className={'logout-window'}>
                    <button disabled={disabled} onClick={signOut} className={'logout'}>
                        logout
                    </button>
                </div>
                    :
                    <></>
            }
        </div>
    );
};

export default UsernameBlock;
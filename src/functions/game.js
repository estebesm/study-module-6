
const getMixedNumbersOfArray = (array) => {
    let nums = array,
        ranNums = [],
        i = nums.length,
        j = 0;

    while (i--) {
        j = Math.floor(Math.random() * (i+1));
        ranNums.push(nums[j]);
        nums.splice(j,1);
    }
    return ranNums
}

export const getExercise = (mode) => {
    const [cols, rows] = mode.split('x')
    const size = cols * rows
    const array = []
    for (let i = 1; i<=size; i++){
        array.push(i)
    }
    const newArr = array.map(item => item % (size/2) + 1)

    return getMixedNumbersOfArray(newArr)
}


export const modes = ['4x3', '4x4', '6x4']

export const addUsernameToLocalStorage = username => {
    localStorage.setItem('username', username)
}

export const removeUsernameFromLocalStorage = () => {
    localStorage.removeItem('username')
}

export const getUsernameFromLocalStorage = () => localStorage.getItem('username')

export const getLeaderboard = () => {
    let leaderboard = JSON.parse(localStorage.getItem('leaderboard'))
    if(leaderboard){
        return leaderboard
    }
    leaderboard = {}
    modes.forEach(mode => {
        leaderboard[mode] = []
    })
    localStorage.setItem('leaderboard', JSON.stringify(leaderboard))
    return leaderboard
}

export const getLeaderboardSortedByTime = () => {
    const leaderboard = getLeaderboard()
    modes.forEach(mode => {
        leaderboard[mode].sort((a, b) => a.time - b.time)
    })
    return leaderboard
}

export const getLeaderboardSortedBySteps = () => {
    const leaderboard = getLeaderboard()
    modes.forEach(mode => {
        leaderboard[mode].sort((a, b) => a.steps - b.steps)
    })
    return leaderboard
}

const isBestResult = (mode, score) => {
    const leaderboard = getLeaderboard()
    if(!leaderboard[mode].find(res => res.username === getUsernameFromLocalStorage())){
        return true
    }

    const bestScore = leaderboard[mode].find(res => res.username === getUsernameFromLocalStorage())

    return score < bestScore.score

}

export const addResultToLocalStorage = (mode, steps, time) => {
    const leaderboard = getLeaderboard()

    if(isBestResult(mode, steps*time)) {
        if(leaderboard[mode].find(res => res.username === getUsernameFromLocalStorage())) {
            leaderboard[mode] = leaderboard[mode].map(res => {
                if (res.username === getUsernameFromLocalStorage()) {
                    return {
                        username: res.username,
                        steps, time,
                        score: steps * time
                    }
                }
                return res
            })
        } else {
            leaderboard[mode] = [
                ...leaderboard[mode],
                {
                    username: getUsernameFromLocalStorage(),
                    steps, time,
                    score: steps * time
                }
            ]
        }
    }
    leaderboard[mode].sort((a, b) => a.score - b.score)
    localStorage.setItem('leaderboard', JSON.stringify(leaderboard))
}
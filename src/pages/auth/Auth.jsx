import React, {useEffect} from 'react';
import './auth.scss'

import AuthForm from "../../components/authForm/AuthForm";

const Auth = (props) => {

    const {signIn} = props

    return (
        <div className={'auth'}>
            <h1>
                Welcome!
            </h1>
            <p>
                Enter your username to continue
            </p>
            <AuthForm signIn={signIn}/>
        </div>
    );
};

export default Auth;
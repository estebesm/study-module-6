import React, {useEffect, useState} from 'react';
import './game.scss'
import {useParams} from "react-router-dom";
import {getExercise} from "../../functions/game";
import Card from "../../components/card/Card";
import GameMenu from "../../components/gameMenu/GameMenu";
import ResultWindow from "../../components/resultWindow/ResultWindow";
import {addResultToLocalStorage, modes} from "../../functions/localStorage";

const Game = () => {
    const params = useParams()
    const cols = params.mode.split('x')[0]

    const background = {}
    background[modes[0]] = 'steelblue'
    background[modes[1]] = '#ff9800'
    background[modes[2]] = '#607d8b'

    const [cards, setCards] = useState(getExercise(params.mode))
    const [activeCards, setActiveCards] = useState([])
    const [foundCards, setFoundCards] = useState([])
    const [disabled, setDisabled] = useState(false)

    const [steps, setSteps] = useState(0)
    const [time, setTime] = useState(0)

    const [resultWindowOpened, setResultWindowOpened] = useState(false)

    const restartGame = () => {
        setSteps(0)
        setTime(0)
        setActiveCards([])
        setFoundCards([])
        setCards(getExercise(params.mode))
    }

    useEffect(() => {
        if(activeCards.length === 2){
            setSteps(steps+1)
            if(cards[activeCards[0]] === cards[activeCards[1]]){
                setFoundCards([...foundCards, ...activeCards])
            }
            setDisabled(true)
            setTimeout(() => {
                setActiveCards([])
                setDisabled(false)
            }, 1000)
        }
    }, [activeCards])

    useEffect(() => {
        if(foundCards.length === cards.length){
            setTimeout(() => {
                setResultWindowOpened(true)
            }, 1000)
        }
    }, [foundCards])

    useEffect(() => {
        if(resultWindowOpened) {
            addResultToLocalStorage(params.mode, steps , time)
        } else{
            const interval = setInterval(() => {
                setTime(time => time + 1)
            }, 1000)
            return () => clearInterval(interval)
        }
    }, [resultWindowOpened])


    return (
        <div className={'game'} style={{
            background: background[params.mode]
        }}>
            <div className={'container'}>
                <GameMenu restartGame={restartGame}/>
                <ResultWindow opened={resultWindowOpened}
                              closeWindow={() => {
                                  document.body.style.overflow = 'visible'
                                  setResultWindowOpened(false)
                              }}
                              steps={steps}
                              time={time}
                              restartGame={restartGame}
                              mode={params.mode}
                />
                <div className={'game__board'}>
                    <div className={'game-info'}>
                        <h2 className={'score'}>
                            STEPS: {steps}
                        </h2>
                        <h2 className={'timer'}>
                            TIME: {time}
                        </h2>
                    </div>
                    <div className={'cards'} style={{
                        gridTemplateColumns: `repeat(${cols}, 1fr)`
                    }}>
                        {
                            cards.map((card, index) =>
                                <Card card={card}
                                      activeCards={activeCards}
                                      setActiveCards={setActiveCards}
                                      foundCards={foundCards}
                                      index={index}
                                      disabled={disabled}
                                      key={`game__card__${index}`}
                                />
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Game;
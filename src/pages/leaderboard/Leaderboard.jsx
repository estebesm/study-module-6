import React, {useId, useState} from 'react';
import './leaderboard.scss'
import {getLeaderboard, modes} from "../../functions/localStorage";
import {useNavigate} from "react-router-dom";
import SortWindow from "../../components/sortWindow/SortWindow";

const Leaderboard = () => {
    const navigate = useNavigate()
    const navigationId = useId()
    const [leaderboard, setLeaderboard] = useState(getLeaderboard())
    const [activeMode, setActiveMode] = useState(modes[0])
    const [activeSortItem, setActiveSortItem] = useState('score')

    return (
        <div className={'leaderboard'}>
            <div className={'container'}>
                <button onClick={() => navigate('/')} className={'open-menu'}>
                    MENU
                </button>
                <div className={'navigation'}>
                    <ul>
                        {
                            modes.map((mode, index) => (
                                <li key={`${navigationId}_${index}`}
                                    className={mode === activeMode ? 'active' : ''}>
                                    <button onClick={() => setActiveMode(mode)}>
                                        {mode}
                                    </button>
                                </li>
                            ))
                        }
                    </ul>
                </div>
                <div className={'table'}>
                    <SortWindow setLeaderBoard={setLeaderboard}
                                activeSortItem={activeSortItem}
                                setActiveSortItem={setActiveSortItem}
                    />
                    {
                        leaderboard[activeMode].length > 0 ?
                            <ul>
                                <li className={'title'}>
                                    <span className={'item'}>
                                        username
                                    </span>
                                    <span className={'item'}>
                                        {activeSortItem}
                                    </span>
                                </li>
                                {
                                    leaderboard[activeMode].map((result, index) => (
                                        <li key={`leaderboard__${index}`}>
                                            <span className={'position'}>
                                                {index+1}.
                                            </span>
                                            <span className={'item'}>
                                                {result.username}
                                            </span>
                                            <span className={'item'}>
                                                {result[activeSortItem]}
                                            </span>
                                        </li>
                                    ))
                                }
                            </ul>
                            :
                            <div className={'empty-list'}>
                                <h3>No results</h3>
                            </div>
                    }
                </div>
            </div>
        </div>
    );
};

export default Leaderboard;
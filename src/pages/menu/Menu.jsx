import React, {useState} from 'react';
import './menu.scss'
import {useNavigate} from "react-router-dom";
import UsernameBlock from "../../components/usernameBlock/UsernameBlock";
import {modes} from "../../functions/localStorage";
import logo from '../../images/memory-logo.png'

const Menu = (props) => {
    const {signOut} = props
    const navigate = useNavigate()

    const [activeMode, setActiveMode] = useState(modes[0])
    return (
        <div className={'menu'}>
            <div className={'container'}>
                <UsernameBlock signOut={signOut}/>
                <img src={logo} className={'logo'} alt={'memory'}/>
                <ul className={'menu__list'}>
                    <li className={'list__item'}>
                        <button className={'start'}
                                onClick={() => {
                                    navigate(`/game/${activeMode}`)
                                }}>
                            START
                        </button>
                    </li>
                    <li className={'list__item'}>
                        <span className={'mode'}>

                        </span>
                        <div className={'mode__buttons'}>
                            {modes.map((mode, index) =>
                                <button className={mode === activeMode ? 'active' : ''}
                                        onClick={() => setActiveMode(mode)}
                                        key={`menu__menu-list__button__${index}`}
                                >
                                    {mode}
                                </button>
                            )}
                        </div>
                    </li>
                    <li className={'list__item'}>
                        <button onClick={() => navigate('/leaderboard')}>
                            LEADERBOARD
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default Menu;